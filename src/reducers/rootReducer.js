const initialState =  {
    todos: []
}

function rootReducer( state = initialState, action){
    switch(action.type) {
        case 'TODO_FETCHED':
            return {...state, todo: action.todo};
        case 'TODOS_FETCHED':
            const list = action.todos
            return {...state, todos: list};
        case 'UPDATE_TODO_SUCCESS':
            console.log("UPDATE_TODO_SUCCESS");
            break;
        case 'UPDATE_TODO_FAIL':
            console.log("UPDATE_TODO_FAIL", action.payload);
            break;
        default:
            return state;
    }
}

export default rootReducer;