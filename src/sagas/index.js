import { put, takeLatest, all } from 'redux-saga/effects';
import axios from 'axios';

function* fetchTodos() {
    let todos = []
    yield axios.get("http://localhost:4000/").then(res => todos = res.data);
    yield put({
        type: 'TODOS_FETCHED',
        todos: todos
    })
}

function* fetchTodo(action) {
    let todo;
    yield axios.get(`http://localhost:4000/${action.id}`).then(res =>  todo = res.data);
    yield put({
        type: 'TODO_FETCHED',
        todo: todo
    })
}

function* createTodo(action) {
    console.log("act", action)
    yield axios.post(`http://localhost:4000/create`,action.payload, {
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json"
        },
      })
      .then((response) => {
        console.log("response",response);
      }, (error) => {
        console.log("error", error);
      });
}

function* updateTodo(action) {
    yield axios.post(`http://localhost:4000/${action.id}`,action.data, {
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json"
        },
      })
      .then((response) => {
        console.log("response",response);
      }, (error) => {
        console.log("error", error);
      });
}

function* deleteTodo(action) {
    console.log(action)
    yield axios.delete(`http://localhost:4000/${action.payload}`).then(res =>  console.log("delete res", res));
    yield put({
        type: 'TODO_DELETED',
    })
}

function* actionWatcher() {
    yield takeLatest('FETCH_TODOS', fetchTodos);
    yield takeLatest('FETCH_TODO', fetchTodo);
    yield takeLatest('UPDATE_TODO', updateTodo);
    yield takeLatest('CREATE_TODO', createTodo);
    yield takeLatest('DELETE_TODO', deleteTodo);
}

export default function* rootSaga() {
    yield all([
        actionWatcher()
    ])
}