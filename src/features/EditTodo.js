import {useState, useEffect} from 'react';
import { useHistory, useRouteMatch } from 'react-router-dom';
import { TodoForm } from './TodoForm';
import { useDispatch, useSelector} from 'react-redux';

export function EditTodo () {
    const [todoState, setTodoState] = useState();
    const dispatch = useDispatch();
    const todo = useSelector(state => state.todo)
    const match = useRouteMatch();
    const history = useHistory();

    useEffect(() => {
        dispatch({
            type: 'FETCH_TODO',
            id: match.params.id
        })
    }, [])

    const onSubmit = async (data) => {
        await dispatch({
            type: 'UPDATE_TODO',
            data,
            id: match.params.id,
        })
        history.push("/");
    }

    return (
        <div className="container">
            <div className="mt-3">
                <h3>Edit Todo</h3>
                {todo ? <TodoForm todo={todo} onSubmit={onSubmit} /> : <div />}
            </div>
        </div>
    )
}