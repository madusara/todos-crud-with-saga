import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { useSelector, useDispatch } from 'react-redux'

export function Todolist () {

    const dispatch = useDispatch();
    const todos = useSelector(state => state.todos);

    useEffect(() => {
        dispatch({
            type: 'FETCH_TODOS',
        })
    }, [todos]) 

    const deleteTodo = (id) => {
        console.log("delete id: ", id);
        dispatch({
            type: 'DELETE_TODO',
            payload: id
        })
    }

    const todosList = (
        todos && todos.map((item) => {
            return (
                <tr key={item._id}>
                <td>{item.text}</td>
                <td className="actionsColumn">
                    <Link to={`/edit/${item._id}`}>Edit</Link>
                    <button className="button" onClick={() => deleteTodo(item._id)}>Delete</button>
                </td>
            </tr>
            )
        })
    )


    return (
        <div className="container">
            <div className="mt-3">
                <h3>Todo List</h3>
                <table className="table table-striped mt-3">
                    <thead>
                        <tr>
                            <th>Text</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {todosList}
                    </tbody>
                </table>
            </div>
        </div>
    )
}