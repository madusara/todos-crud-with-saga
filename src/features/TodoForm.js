import { useForm } from 'react-hook-form';
import { useState, useEffect } from 'react'

export const TodoForm = ({ todo, onSubmit }) => {

    const { register, handleSubmit, setValue } = useForm({
        defaultValues: { text: todo? todo.text : '' }
    });

    useEffect(() => {
        if (todo) {
          setValue( "text", todo.text );
        }
      }, [todo]);

    const submitHandler = handleSubmit((data) => {
        onSubmit(data)
    })

    return (
        <form onSubmit={submitHandler}>
            <div className="form-group">
                <label htmlFor="textInput">Text: </label>
                <input ref={register} type="text" name="text" id="textInput" className="form-control"/>
            </div>
            <div className="form-group">
                <button type="submit" className="btn btn-primary">Save Todo</button>
            </div>
        </form>
    )
}