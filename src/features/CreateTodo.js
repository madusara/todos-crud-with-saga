import { useForm  } from 'react-hook-form';
import { TodoForm } from './TodoForm';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';

export function CreateTodo () {
    const history = useHistory();
    const dispatch = useDispatch();

    const onSubmit = async (data) => {
        // console.log("create todo", JSON.stringify(data))
        // await createTodo(data);
        await dispatch({
            type: 'CREATE_TODO',
            payload: data
        })
        history.push("/");
    }

    return (
        <div className="container">
            <div className="mt-3">
                <h3>Create Todo</h3>
                <TodoForm onSubmit={onSubmit} />
            </div>
        </div>
    )
}