import {Link, Switch, Route} from 'react-router-dom';
import { Todolist } from './features/Todolist';
import { EditTodo } from './features/EditTodo';
import { CreateTodo } from './features/CreateTodo';

function App() {
  return (
    <div>
      <nav className="navbar navbar-light navbar-expand-lg bg-light">
        <ul className="navbar-nav mr-auto">
          <li className="navbar-item">
            <Link to="/" className="nav-link">Todo List</Link>
          </li>
          <li className="navbar-item">
            <Link to="/create" className="nav-link">Create todo</Link>
          </li>
        </ul>
      </nav>
      <Switch>
        <Route exact path="/" component={Todolist} />
        <Route exact path="/edit/:id" component={EditTodo} />
        <Route exact path="/create" component={CreateTodo} />
      </Switch>
    </div>
  );
}

export default App;
